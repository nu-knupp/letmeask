import firebase from 'firebase/app';

import 'firebase/auth';
import 'firebase/database';

// .env.local não funcionou

const firebaseConfig = {
    apiKey: "AIzaSyCEdy3zndLmmcQZ86tj5YZxKd34Za-NO-o",
    authDomain: "letmeask-103cc.firebaseapp.com",
    databaseURL: "https://letmeask-103cc-default-rtdb.firebaseio.com",
    projectId: "letmeask-103cc",
    storageBucket: "letmeask-103cc.appspot.com",
    messagingSenderId: "834345420573",
    appId: "1:834345420573:web:8fd3bab6e4e37fa5bc43f4"
}

/* const firebaseConfig = {
    apiKey: process.env.REACT_APP_API_KEY,
    authDomain: process.env.REACT_APP_AUTH_DOMAIN,
    databaseURL: process.env.REACT_APP_DATABASE_URL,
    projectId: process.env.REACT_APP_PROJECT_ID,
    storageBucket: process.env.REACT_APP_STORAGE_BUCKET,
    messagingSenderId: process.env.REACT_APP_MASSAGING_SENDER_ID,
    appId: process.env.REACT_APP_ID
};

.env.local

# firebase
REACT_APP_API_KEY="AIzaSyCEdy3zndLmmcQZ86tj5YZxKd34Za-NO-o",
REACT_APP_AUTH_DOMAIN="letmeask-103cc.firebaseapp.com",
REACT_APP_DATABASE_URL="https://letmeask-103cc-default-rtdb.firebaseio.com",
REACT_APP_PROJECT_ID="letmeask-103cc",
REACT_APP_STORAGE_BUCKET="letmeask-103cc.appspot.com",
REACT_APP_MASSAGING_SENDER_ID="834345420573",
REACT_APP_ID="1:834345420573:web:8fd3bab6e4e37fa5bc43f4" */

firebase.initializeApp(firebaseConfig);

const auth = firebase.auth();
const database = firebase.database();

export { firebase, auth, database }